# DevOps practical exercise

This repo contains the various practical exercises for our DevOps engineers.

Each branch is aimed at a different level of candidate or engineer, so ensure you fork/clone your own version at the correct level.
```
git clone git@bitbucket.org/waracle/devops
git checkout junior
```

Each branch will contain a README which describes the exercise in detail.

## Submission

The idea is that you will follow the Waracle development methodology which essentially means developing a solution for the task.  You will be continually committing and pushing your changes at appropriate times.  This increases the '[bus factor](https://en.wikipedia.org/wiki/Bus_factor)' on the project.

When you have completed your solution, raise a Pull Request (PR) to merge your branch into ours.  The PR forms the basis of a code review, where the senior members of project will review, and where appropriate, suggests improvements, etc.  It is normal and encouraged to take the comments, suggestions as entries into the feedback cycle, so don't be disheartened as the feedback serves to improve the code quality specifically, and the project generally.
