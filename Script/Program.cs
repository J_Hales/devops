﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Configuration;
using System.Reflection;
using Amazon.S3;
using Amazon.S3.Model;

namespace BitBucket_AWS_Automation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("BitBucket_AWS_Automation v1.02 - Jordan Hales \n");
            Console.Write("Process for downloading, unzipping and then uploading a file from Google Drive to an AWS S3 Bucket started. \n");
            // Checks whether to use environmental variables given or default variables
            EnvironmentalVariables environmental = new EnvironmentalVariables();
            environmental.Setup();

            S3Uploader uploader = new S3Uploader();
            DriveDownloader downloader = new DriveDownloader();

            // Gets the current directiory that the exe is being executed from
            var exePath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            // Checks that getting the path has been successful
            if (exePath == null)
            {
                throw new ArgumentOutOfRangeException("The application was unable to determine the current path. Now exiting.");
            }
            else
            {
                // Begins the application
                Console.Write("Path successfully determined. \n");
                downloader.Download(exePath.ToString());
                downloader.Extract(exePath.ToString());
                uploader.UploadFile(exePath.ToString());

                // Tidy any created items
                Console.Write("Attempting to tidy local files... \n");
                downloader.Clean(exePath.ToString());
                Console.Write("Application completed. Now exiting.");
            }
        }

        public class EnvironmentalVariables
        {
            string[] keys = { "GoogleDriveID", "ZipFilePassword", "AWSBucketName", "AWSProfileName", "AWSAccessKey", "AWSSecretKey", "AWSRegion" };

            public void Setup()
            {
                // For every variable needed in the application
                foreach (string entry in keys)
                {
                    // If there is no environmental variable available, set it to default via App.config
                    if (System.Environment.GetEnvironmentVariable(entry) == null)
                    {
                        System.Environment.SetEnvironmentVariable(entry, ConfigurationSettings.AppSettings[entry]);
                    }
                }
            }

            public void Tidy()
            {
                // Deletes all the environmental variables made for this application
                foreach (string entry in keys)
                {
                    System.Environment.SetEnvironmentVariable(entry, null);
                }
            }
        }
        public class DriveDownloader
        {
            private string driveID = System.Environment.GetEnvironmentVariable("GoogleDriveID");
            private string drivePrefix = "https://drive.google.com/uc?export=download&id=";

            public void Download(string EP)
            {
                // Downloads a file from a given link and stores it in the same location that the application has been executed from
                Console.Write("Attempting to download the file from google drive... \n");
                WebClient wc = new WebClient();
                var driveLink = drivePrefix + driveID;

                // Outputs a file at the same place the application is executed with the given name
                var zipOutput = EP + @"\DriveDownload.zip";

                // Attempts to download the file
                try
                {
                    wc.DownloadFile(driveLink, zipOutput);
                }
                catch (System.Net.WebException)
                {
                    Console.Write("The application was unable to download the file. Exiting the application.");
                }
                finally
                {
                    Console.Write("Download Successful. \n");
                }
            }

            public void Extract(string EP)
            {
                // Extracts the downloaded file in sub-folder of the same directory the application is executed
                Console.Write("Attempting to extract the file... \n");
                var extractor = new ICSharpCode.SharpZipLib.Zip.FastZip();
                extractor.Password = System.Environment.GetEnvironmentVariable("ZipFilePassword");

                // Attempts to extract the file with the given variables
                extractor.ExtractZip(EP + @"\DriveDownload.zip", EP + @"\Extracted", "");

                // Checks that a new folder was created
                if (Directory.Exists(EP) == false)
                {
                    throw new ArgumentOutOfRangeException("Zip file was not created. Exiting the application.");
                }
                else
                {
                    Console.Write("Extraction Successful. \n");
                }
            }

            public void Clean(string EP)
            {
                // Deletes the locally stored files
                File.Delete(EP + @"\DriveDownload.zip");
                Directory.Delete(EP + @"\Extracted", true);
            }
        }

        public class S3Uploader
        {
            // Uploads the extracted folder to a given AWS S3 Bucket
            private string bucketName = System.Environment.GetEnvironmentVariable("AWSBucketName");
            private Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(System.Environment.GetEnvironmentVariable("AWSRegion"));

            public void UploadFile(string EP)
            {
                // Uses the AWSSDK to upload the extracted folder to the given bucket
                Console.Write("Attempting to upload the extracted zip file to the AWS S3 Bucket... \n");
                var filePath = EP + @"\Extracted";
                var client = new AmazonS3Client(region);
                var transferUtility = new Amazon.S3.Transfer.TransferUtility(client);

                try
                {
                    // Uploads everything in the extracted folder, keeping the folder structers inside
                    transferUtility.UploadDirectory(filePath, bucketName, "*.*", SearchOption.AllDirectories);
                }
                catch (Exception ex)
                {
                    Console.Write("The application was unable to upload the extracted zip file. Exiting the application.");
                }
                finally
                {
                    Console.Write("Upload processed successfully. \n");
                }
            }
        }
    }
}
